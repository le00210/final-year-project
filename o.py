from rdflib import Graph, plugin
from rdflib.serializer import Serializer

json = """
[{"@id": "value"}]
"""

g = Graph().parse(data=json, format='json-ld')

xml = g.serialize(format='xml', indent=4).decode()
print(xml)
