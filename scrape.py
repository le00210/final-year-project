from recipe_scrapers import scrape_me
import urllib.request
import requests
from bs4 import BeautifulSoup
import json

def scrapeIngredients():
    urls = ["https://www.bbc.co.uk/food/ingredients/a-z/a/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/a/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/b/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/b/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/b/3",
            "https://www.bbc.co.uk/food/ingredients/a-z/b/4",
            "https://www.bbc.co.uk/food/ingredients/a-z/c/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/c/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/c/3",
            "https://www.bbc.co.uk/food/ingredients/a-z/c/4",
            "https://www.bbc.co.uk/food/ingredients/a-z/c/5",
            "https://www.bbc.co.uk/food/ingredients/a-z/c/6",
            "https://www.bbc.co.uk/food/ingredients/a-z/c/7",
            "https://www.bbc.co.uk/food/ingredients/a-z/d/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/d/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/e/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/f/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/f/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/g/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/g/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/g/3",
            "https://www.bbc.co.uk/food/ingredients/a-z/h/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/h/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/i/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/j/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/k/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/l/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/l/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/l/3",
            "https://www.bbc.co.uk/food/ingredients/a-z/m/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/m/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/m/3",
            "https://www.bbc.co.uk/food/ingredients/a-z/n/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/o/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/p/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/p/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/p/3",
            "https://www.bbc.co.uk/food/ingredients/a-z/p/4",
            "https://www.bbc.co.uk/food/ingredients/a-z/p/5",
            "https://www.bbc.co.uk/food/ingredients/a-z/q/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/r/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/r/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/r/3",
            "https://www.bbc.co.uk/food/ingredients/a-z/s/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/s/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/s/3",
            "https://www.bbc.co.uk/food/ingredients/a-z/s/4",
            "https://www.bbc.co.uk/food/ingredients/a-z/s/5",
            "https://www.bbc.co.uk/food/ingredients/a-z/t/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/t/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/u/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/v/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/w/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/w/2",
            "https://www.bbc.co.uk/food/ingredients/a-z/y/1",
            "https://www.bbc.co.uk/food/ingredients/a-z/z/1"
            ]

def urlList(url):
    # get sitemap with https://stackoverflow.com/questions/10232774/how-to-find-sitemap-xml-path-on-websites
    soup = BeautifulSoup(requests.get(url).text, 'html.parser')
    urls = soup.find_all("a")
    matchingURLs = [url.get("href") for url in urls if "recipes" in url.get("href")]
    return matchingURLs

websites = [
        "http://101cookbooks.com/",
        "http://allrecipes.com/",
        "http://bbc.com/",
        "http://bbc.co.uk/",
        "http://bbcgoodfood.com/",
        "http://bettycrocker.com/",
        "http://bonappetit.com/",
        "https://www.budgetbytes.com/",
        "http://closetcooking.com/",
        "http://cookstr.com/",
        "http://copykat.com/",
        "https://en.wikibooks.org/",
        "http://epicurious.com/",
        "http://finedininglovers.com/",
        "https://food.com/",
        "http://foodnetwork.com/",
        "http://foodrepublic.com/",
        "https://geniuskitchen.com/",
        "https://greatbritishchefs.com/",
        "http://giallozafferano.it/",
        "http://gonnawantseconds.com/",
        "https://healthyeating.nhlbi.nih.gov/",
        "https://heinzbrasil.com.br/",
        "https://www.hellofresh.com/",
        "https://www.hellofresh.co.uk/",
        "https://receitas.ig.com.br/",
        "https://inspiralized.com/",
        "http://jamieoliver.com/",
        "https://www.thekitchn.com/",
        "https://www.matprat.no/",
        "http://mybakingaddiction.com/",
        "https://panelinha.com.br/",
        "http://paninihappy.com/",
        "http://realsimple.com/",
        "https://www.seriouseats.com/",
        "http://simplyrecipes.com/",
        "https://www.southernliving.com/",
        "http://steamykitchen.com/",
        "https://www.tastesoflizzyt.com",
        "http://tastykitchen.com/",
        "http://thepioneerwoman.com/",
        "https://www.thespruceeats.com/",
        "http://thehappyfoodie.co.uk/",
        "http://thevintagemixer.com/",
        "http://tine.no/",
        "http://twopeasandtheirpod.com/",
        "http://whatsgabycooking.com/",
        "http://yummly.com/"
        ]
def createRecipeList(sitemap):
    """
    Generate a list of recipes from a webpage's sitemap
    """
    urls = []
    soup = BeautifulSoup(requests.get(sitemap).text, 'html.parser')
    for url in soup.find_all("url"):
        for loc in url.findChildren("loc"):
            if "recipes" in loc.text:
                urls.append(loc.text)
    return urls

urls = []
for website in [
        "https://allrecipes.com/sitemaps/recipe/1/sitemap.xml",
        "https://bbc.co.uk/food/sitemap.xml",
        "https://bbcgoodfood.com/sitemap.xml",
        "https://greatbritishchefs.com/sitemap.xml"
        ]:
    for i in createRecipeList(website):
        recipe = scrape_me(i)
        filename = recipe.title()
        jsonrecipe = json.loads(json.dumps({
            "title": recipe.title(),
            "image": recipe.image(),
            "rating": recipe.ratings(),
            "total_time": recipe.total_time(),
            "ingredients": recipe.ingredients(),
            "instructions": recipe.instructions().splitlines()}))

        with open("recipes/" + filename + ".json", "w") as filename:
            json.dump(jsonrecipe, filename)

def buildRecipeList():
    for website in websites:
        urlList(website)

urlList("https://bbc.co.uk/food/ingredients")
scrapeIngredients()
