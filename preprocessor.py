import json
import os
import glob
import subprocess
# from jsonschema import validate

def lowerCase(recipe):
    subprocess.call("sed -i 's/.*/\L&/g' recipes/*.json")

# Add a space before and after the character if there isn't one already
# all units
# - 
# For example, converts "10g" to "10 g"
def surround():
    pass
    # parse JSON for each Unit in Ingredients
    # for ingredient in recipes['Ingredients']:
        # unit is any value of key Unit or Alternative in units.json
        # if unit surrounded by spaces, pass; else, add where necessary
  
# Replaces the article "a" with the quantifier 1
def replaceA():
    pass
    # substitute "a" for 1
  
# Figure out from the context what the shorthand refers to
# For example, "bake for 30 minutes at 180 c" refers to celsius, not cups
# Conflicts should be rare and obvious, context-specific knowledge can be hard-coded into the processor
def disambiguateUnits():
    pass
    
# Guess units where they are not specified
# For example, "bake for 30 minutes at 180"
# Success rate might be low but these ocurrences are rare and manual intervention is acceptable
# Use heuristics such as "bake" refers to temperatures within the baking range (say 160 to 250)
# Defaults to manual intervention unless confidence is very high
def addMissingUnits():
    pass

# Rather than using numbers, sometimes they're represented as special characters such as 1/2
# Convert them to normal numbers
def specialChars():
    # eg \u00bd should become 1/2
    pass

# Check that everything in the database conforms to the schema
def validateSchema():
    # for each json file in database/
    validate(schema, file)
    
for filename in glob.glob("recipes/*.json"):
    lowerCase(filename)
    # with open(os.path.join(os.getcwd(), filename), 'r+') as f:
