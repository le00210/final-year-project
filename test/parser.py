def valid():
    parsedInput = input.preprocess().tokenise().parse()
    return parsedInput.isValid()

input = "beat the butter and sugar with an electric whisk until well combined"
valid(input)

# parsedInput should be JSON eg. {"beat", "verb"}
# input:    beat the butter and sugar with an electric whisk until well combined
# parsed:   verb article ingredient conjunction ingredient preposition article utensil preposition adverb verb

# input:    To make the buttercream, beat the butter, icing sugar and vanilla with an electric whisk until pale and creamy
# parsed:   preposition verb article mixture verb article ingredient ingredient conjunction ingredient preposition article utensil preposition adjective conjunction adjective
