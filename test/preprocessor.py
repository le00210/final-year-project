# Convert 10g to 10 g
# Should work for any character after any digit
# Should not add an extra space if there already is one
def testSeparateUnits():
  # should work for every unit in Units
  # should work with any digit 0-9 but not with letters, eg. "10fg" should remain unchanged
  # it will later be determined whether typo correction is within the scope
  # should not add an extra space if there already is one
  # should work for every example in recipes/*.json