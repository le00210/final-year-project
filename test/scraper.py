import requests
from bs4 import BeautifulSoup
sitemap = "http://101cookbooks.com/"
soup = BeautifulSoup(requests.get(sitemap).text, 'html.parser')
urls = soup.find_all("a")
for url in urls:
    print(url.get("href"))
