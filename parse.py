from lark import Lark, tree, Transformer, v_args
from anytree.exporter import JsonExporter
from anytree.importer import DictImporter
import json

class TreeToJson(Transformer):
    def list(self, items):
        dictionary = {}
        for i in items:
            dictionary[i.name] = i.value
        return dictionary

    def pair(self, key_value):
        name, value = key_value
        return name, value

    def object(self, items):
        name = items[0]
        try:
            elements = items[1]
        except indexError:
            elements = []

        return {name, elements}

    def string(self, s):
        (s,) = s
        return s[1:-1]

with open('grammar.lark', 'r') as grammar:
    parser = Lark(grammar, start='start')

filename = 'recipes/.json'
with open(filename, 'r') as recipe:
    for section in ["ingredients", "instructions"]:
        for i in json.load(recipe)[section]:
            # json.loads(parser.parse(i).pretty())
            # print(parser.parse(i))
            tree = parser.parse(i)
            print(TreeToJson().transform(tree))
#           print(parser.parse(i).pretty())
            # tree.pydot__tree_to_png(parser.parse(i), 'ast.png')
