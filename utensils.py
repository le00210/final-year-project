import urllib.request
import requests
from bs4 import BeautifulSoup

url = "https://en.wikipedia.org/wiki/List_of_food_preparation_utensils"
soup = BeautifulSoup(requests.get(url).text, 'html.parser')
for i in soup.select("th[scope=row] > a"): # > th > a"):
    print(i.text)
