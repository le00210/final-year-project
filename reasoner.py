from owlready2 import *

ontology = get_ontology("file:///home/liam/final-year-project/bbc.ttl").load()

with ontology:
    # owlready2.readthedocs.io/en/latest/onto.html
    def hasAuthor(author):
        pass

    def hasName(name):
        pass

    def hasAllergen(allergen):
        pass

    def hasDifficulty(difficulty):
        pass

    def hasIngredient(ingredient):
        ontology.search(iri = "*ingredient")

    def hasVegetables():
        pass

    sync_reasoner()
