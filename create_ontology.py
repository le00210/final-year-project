from rdflib import Graph, plugin
from rdflib.serializer import Serializer
import json, rdflib_jsonld

# The context is to define shorthands for things so that for example
# name refers to https://schema.org/name
context = """
{
    "name": "file:///schema.org/name",
    "image": {
        "@id": "http://schema.org/image",
        "@type": "id"
    }
}
"""

other = """
    "name": "http://rdf.data-vocabulary.org/#name",
    "author": "https://rdf.data-vocabulary.org/#author",
    "collection": "http://rdf.data-vocabulary.org/#collection",
    "menu": "http://rdf.data-vocabulary.org/#menu",
    "ingredientList": "http://rdf.data-vocabulary.org/#ingredientList",
    "ingredient": "http://rdf.data-vocabulary.org/#ingredients",
    "yield": "http://rdf.data-vocabulary.org/#yield",
    "instructions": "http://rdf.data-vocabulary.org/#instructions",
    "stageSequence": "http://rdf.data-vocabulary.org/#stageSequence",
    "sequence": {
      "@id": "http://rdf.data-vocabulary.org/#sequence",
      "@type": "xsd:integer"
      },
    "unit": "http://rdf.data-vocabulary.org/#unit",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
}
"""

recipe = """
{
    "@context": "http://schema.org/",
    "@type": "Person",
    "name": "Mojito"
}
"""

other = """
    "author": "",
    "ingredientList": [
    "1/2 cup fresh mint leaves"],
    "instructions": [
    "Crush blah blah",
    "Step 2"]
}
"""

g = Graph().parse(data=recipe, format='json-ld')

jsonld = g.serialize(format='xml', destination="ontology.xml", indent=4)

bbc = """
@prefix dc:  <http://purl.org/dc/elements/1.1/> .

<> dc:title "Ontology" .

@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl:  <http://www.w3.org/2002/07/owl#> .

@prefix : <#> .

:hasAllergens a rdfs:Property.
:hasName a rdfs:Property.
:hasWebsite a rdfs:Property.
:hasAuthor a rdfs:Property.
:hasRating a rdfs:Property.
:hasDifficulty a rdfs:Property.
:hasIngredient a rdfs:Property.
"""
